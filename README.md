# merkle-proof-format-analysis
Generate 10000 proofs randomly, encode using each encoder (v1, v2, Repr), and compare each size of encoded byte sequences.

The details of encoders are [here](https://hackmd.io/_bQ9Spe0QOmmqnJK2tjWCg?view).
Note that there are 4 types of proofs (2 types of Irmin Trees(32-tree and binary tree) * 2 types of merkle-proofs(tree_proof and stream_proof)) and we generated 10000 proofs randomly for each of 4 types (40000 in total).

# Details
Proofs are generated using `Proof[2|32].Gen` module in [test_merkle_proof](https://gitlab.com/tezos/tezos/-/blob/master/src/lib_context/test/test_merkle_proof.ml)

The test commit is [here](https://gitlab.com/dailambda/tezos/-/commit/b63a51b19d8329f7a554cdb28451537ddd583a22)

# Results
Below graphs contains 10000 dots, `y=x` line, and `y=ax` line.
Each dot show the encoded byte length for generated proofs using 2 encoders.
Two lines are drawn for references.
`a` is determinded by fitting using `fit` in gnuplot.

## 32-tree-proof
![32-tree-proof](./resources/random32_tree0.svg)
![32-tree-proof](./resources/random32_tree1.svg)
![32-tree-proof](./resources/random32_tree2.svg)

## 32-stream-proof
![32-stream-proof](./resources/random32_stream0.svg)
![32-stream-proof](./resources/random32_stream1.svg)
![32-stream-proof](./resources/random32_stream2.svg)

## 2-tree-proof
![2-tree-proof](./resources/random2_tree0.svg)
![2-tree-proof](./resources/random2_tree1.svg)
![2-tree-proof](./resources/random2_tree2.svg)

## 2-stream-proof
![2-stream-proof](./resources/random2_stream0.svg)
![2-stream-proof](./resources/random2_stream1.svg)
![2-stream-proof](./resources/random2_stream2.svg)

# Consideration
For any types of proofs, v2 encodes it shorter than repr, and repr encodes it shorter than v1.
