prefix = "./"
fn = "random2_tree"
tname = "2-tree-proof"

set datafile separator "\t"

set term svg

set format x "%g"
set format y "%g"

set output prefix . fn . "0.svg"
set xlabel 'V1 Encofing Bytes'
set ylabel 'V2 Encoding Bytes'

f(x)=a*x
fit f(x) prefix . fn . ".tsv" using 1:2 via a
ti = sprintf("y=%.3fx", a)

plot prefix . fn . ".tsv" with points ls 0 pointsize 10 linecolor black title tname,\
x with lines linecolor rgb "#00aa88" lt -1 title "y=x",\
f(x) with lines linecolor rgb"#aa0088" title ti

set output prefix . fn . "1.svg"
set xlabel 'V1 Encofing Bytes'
set ylabel 'REPR Bytes'

fit f(x) prefix . fn . ".tsv" using 1:3 via a
ti = sprintf("y=%.3fx", a)

plot prefix . fn. ".tsv" using 1:3 with points ls 0 pointsize 10 linecolor black title tname,\
x with lines linecolor rgb "#00aa88" lt -1 title "y=x",\
f(x) with lines linecolor rgb"#aa0088" title ti

set output prefix . fn. "2.svg"
set xlabel 'V2 Encofing Bytes'
set ylabel 'REPR Bytes'

fit f(x) prefix . fn . ".tsv" using 2:3 via a
ti = sprintf("y=%.3fx", a)

plot prefix . fn . ".tsv" using 2:3 with points ls 0 pointsize 10 linecolor black title tname,\
x with lines linecolor rgb "#00aa88" lt -1 title "y=x",\
f(x) with lines linecolor rgb"#aa0088" title ti
